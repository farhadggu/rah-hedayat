from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _



class KheiriePanelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    verbose_name = _("تنظیمات سایت")
    name = 'Kheirie_Panel'
