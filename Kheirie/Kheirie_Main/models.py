from django.db import models
from django.utils.html import format_html
from ckeditor_uploader.fields import RichTextUploadingField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from translated_fields import TranslatedField
from django.utils.translation import gettext_lazy as _



class PostManager(models.Manager):
    def published(self):
        return self.filter(status='p')


class Category(MPTTModel):
    parent = TreeForeignKey('self', blank=True, null=True, on_delete=models.CASCADE, related_name="children", verbose_name=_("زیر مجموعه"))
    title = TranslatedField(models.CharField(_("تیتر دسته بندی"), max_length=100, help_text="کادر اول فارسی کادر دوم انگلیسی"))
    slug = models.SlugField(_("نام دسته بندی در لینک(اسلاگ)"), max_length=100)
    status = models.BooleanField(_("وضعیت"), default=True)
    # Post Models
    title_post = TranslatedField(models.CharField(_("تیتر مقاله"), max_length=200, help_text = "کادر اول فارسی کادر دوم انگلیسی", blank=True))
    desc_post =  TranslatedField(RichTextUploadingField(_("توضیحات مقاله"), help_text = "کادر اول فارسی کادر دوم انگلیسی", blank=True))
    img_post = models.ImageField(_("عکس"), upload_to='media', blank=True, null=True)
    video_post = models.URLField(_("لینک کد امبد آپارات"), max_length=500, blank=True, null=True)
    download = models.FileField(_("آپلود فایل"), upload_to="documents" ,blank=True, null=True)



    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("دسته بندی")
        verbose_name_plural = _("دسته بندی ها")
        ordering = ['-id']



    def __str__(self):
        full_path = [self.title]
        k = self.parent
        while k is not None:
            full_path.append(k.title)
            k = k.parent
        return ' / '.join(full_path[::1])



class Post(models.Model):
    STATUS = (
        ('p', 'Publish'),
        ('d', 'Draft'),
    )

    category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.CASCADE, related_name="articles", verbose_name=_("دسته بندی"))
    title = TranslatedField(models.CharField(_("تیتر مقاله"), max_length=200, help_text="کادر اول فارسی کادر دوم انگلیسی"))
    slug = models.SlugField(_("نام مقاله در لینک (اسلاگ)"), )
    desc =  TranslatedField(RichTextUploadingField(_("توضیحات مقاله"), help_text="کادر اول فارسی کادر دوم انگلیسی"))
    img = models.ImageField(_("عکس"), upload_to='media', blank=True, null=True)
    video = models.URLField(_("لینک کد امبد آپارات"), max_length=500, blank=True, null=True)
    status = models.CharField(_("وضعیت"), max_length=1, choices=STATUS)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)


    class Meta:
        verbose_name = _("مقاله")
        verbose_name_plural = _("مقالات")
        ordering = ['-id']
        


    def __str__(self):
        return self.title


    def img_tag(self):
        return format_html('<img src="{}" width=100px />'.format(self.img.url))
    img_tag.short_description = "Image"

    objects = PostManager()