# Generated by Django 3.2 on 2021-05-03 04:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Kheirie_Main', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='title_en',
            new_name='title',
        ),
        migrations.RemoveField(
            model_name='category',
            name='title_fa',
        ),
    ]
