from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _



class KheirieMainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    verbose_name = _("مقالات")
    name = 'Kheirie_Main'
