from django.urls import path
from django.views.static import serve
from django.conf.urls import url
from Kheirie import settings
from .views import header, footer, home_page, DetailPage, PostList, category_page, about_us, contact_us


app_name = 'kheirie'
urlpatterns = [
    path('header', header, name="header"),
    path('footer', footer, name="footer"),
    path('', home_page, name='home'),
    path("detail/<slug:slug>/", DetailPage.as_view(), name="detail"),
    path("all-posts/", PostList.as_view(), name="all-posts"),
    path('category/<slug:slug>', category_page, name="category"),
    path('about-us', about_us, name="about-us"),
    path('contact-us', contact_us, name="contact-us"),
    url(r'^download/(?P<path>.*)$',serve,{'document_root':settings.MEDIA_ROOT}),
]
