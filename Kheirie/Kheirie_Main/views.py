from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView
from .models import Post, Category
from Kheirie_Panel.models import Settings
from django.utils.translation import activate


# Header Code Behind

def header(request):
    settings = get_object_or_404(Settings)
    category = Category.objects.filter(status=True)
    context = {
        'settings' : settings,
        'category' : category,
    }
    return render(request, 'shared/Header.html', context)


# Footer Code Behind

def footer(request):
    settings = get_object_or_404(Settings)
    category = Category.objects.filter(status=True)
    context = {
        'settings' : settings,
        'category' : category,

    }
    return render(request, 'shared/Footer.html', context)


def home_page(request):
    post = Post.objects.published().order_by('-id')[:9]
    category = Category.objects.filter(status=True)
    settings = get_object_or_404(Settings)
    context = {
        'object_list': post,
        'settings' : settings,
        'category': category
    }
    return render(request, 'home_page.html', context)



class DetailPage(DetailView):
    def get_object(self):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Post.objects.published(), slug=slug)

    template_name = 'detail_page.html'


class PostList(ListView):
    queryset = Post.objects.published().order_by('-id')
    template_name = 'post_list.html'


def category_page(request, slug):
    category = get_object_or_404(Category, slug=slug)
    context = {
        'cat': category,
    }
    return render(request, 'category_page.html', context)


def about_us(request):
    settings = get_object_or_404(Settings)
    context = {
        'settings' : settings,
    }
    return render(request, 'about_us.html', context)


def contact_us(request):
    settings = get_object_or_404(Settings)
    context = {
        'settings' : settings,
    }
    return render(request, 'contact_us.html', context)


def change_lang(request):
    activate(request.GET.get('lang'))
    return redirect(request.GET.get('next'))