from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from Kheirie import settings
from Kheirie_Main.views import change_lang
from django.conf.urls.i18n import i18n_patterns


urlpatterns = [
    path('change_lang', change_lang, name="change_lang")
]   

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),

    path('', include('Kheirie_Main.urls')),

)


urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)